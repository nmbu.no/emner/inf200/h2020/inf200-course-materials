# -*- encoding: utf-8 -*-

"""
"""

__author__ = 'Hans Ekkehard Plesser / NMBU'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def square(x):
    return x * x
