# -*- encoding: utf-8 -*-

"""
Code for working with palindromes.
"""

__author__ = 'Hans Ekkehard Plesser / NMBU'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def is_palindrome(text):
    """
    Returns True if argument is palindrome.

    >>> is_palindrome('')
    True

    >>> is_palindrome('a')
    True

    >>> is_palindrome('anna')
    True

    >>> is_palindrome('ali')
    False

    """

    return text == text[::-1]