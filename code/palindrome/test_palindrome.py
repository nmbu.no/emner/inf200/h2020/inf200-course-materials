# -*- encoding: utf-8 -*-

"""
Tests for palindrome program.
"""

__author__ = 'Hans Ekkehard Plesser / NMBU'
__email__ = 'hans.ekkehard.plesser@nmbu.no'

from palindrome import is_palindrome


def test_empty():
    assert is_palindrome('')


def test_single():
    assert is_palindrome('a')


def test_two_letter_palindrome():
    assert is_palindrome('aa')


def test_two_letter_non_palindrome():
    assert not is_palindrome('ab')


def test_odd_is_p():
    assert is_palindrome('omo')


def test_odd_not_p():
    assert not is_palindrome('oma')


def test_even_is_p():
    assert is_palindrome('anna')


def test_even_not_p():
    assert not is_palindrome('asta')