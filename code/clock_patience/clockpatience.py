# -*- encoding: utf-8 -*-

"""
Simulation of Clock Patience game.
"""

__author__ = 'Hans Ekkehard Plesser / NMBU'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import random

SUITS = ['Hearts', 'Clubs', 'Spades', 'Diamonds']
VALUES = range(1, 14)


def play_game():
    """
    Play single patience game.

    :return: tuple: win/lose, stacks at end of game
    """

    deck = [(s, v) for s in SUITS for v in VALUES]
    random.shuffle(deck)
    stacks = list([] for _ in range(13))
    win = False

    pos = 0
    while deck and not win:
        if stacks[pos] and stacks[pos][-1][1] == VALUES[pos]:
            pos += 1
            if pos > 12:
                pos = 0
            continue

        card = deck.pop()
        stacks[pos].append(card)
        pos += 1
        if pos > 12:
            pos = 0

        win = all(stacks[pos] and stacks[pos][-1][1] == VALUES[pos]
                  for pos in range(len(stacks)))

    return win, stacks


def cp_experiment(num_games):
    """
    Play clock patience repeatedly.

    :param num_games: Number of games to play
    :return: number of games won
    """

    num_won = 0
    for n in range(num_games):
        if n % 100 == 0:
            print(n)
        win, _ = play_game()
        if win:
            num_won += 1

    return num_won

if __name__ == '__main__':
    print(cp_experiment(10000))
