
import pytest

def inv(x):
    if x == 0:
        raise ValueError
    return 1 / x

def test_inv_raises():
    with pytest.raises(ValueError):
        inv(0)
