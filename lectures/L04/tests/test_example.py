
def square(x):
    return x * x

def test_square_1():
    assert square(1) == 1
    
def test_square_2():
    assert square(2) == 3
