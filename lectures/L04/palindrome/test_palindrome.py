
from palindrome import is_palindrome

def test_empty():
    '''Empty string is palindrome.'''
    assert is_palindrome('')
