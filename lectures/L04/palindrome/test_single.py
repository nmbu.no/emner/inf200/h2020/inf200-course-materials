
from palindrome import is_palindrome

def test_single():
    '''Test on single-character text.'''
    
    assert is_palindrome('a')
