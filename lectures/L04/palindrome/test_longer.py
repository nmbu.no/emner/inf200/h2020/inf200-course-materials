
from palindrome import is_palindrome

def test_odd_is():
    '''Odd-length string that is palindrome.'''
    
    assert is_palindrome('abcba')
    
def test_odd_not():
    '''Odd-length string that is not a palindrome.'''
    
    assert not is_palindrome('abcda')
    
def test_even_is():
    '''Even-length string that is palindrome.'''
    
    assert is_palindrome('abba')
    
def test_even_not():
    '''Even-length string that is not a palindrome.'''
    
    assert not is_palindrome('abda')
