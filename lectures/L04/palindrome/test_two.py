
from palindrome import is_palindrome

def test_two_palin():
    '''Test on two-letter string that is palindrome.'''
    
    assert is_palindrome('aa')
    
def test_two_not_palin():
    '''Test on two-letter string that is not palindrome.'''
    
    assert not is_palindrome('ab')
