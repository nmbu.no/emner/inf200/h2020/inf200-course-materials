
def is_palindrome(text):
    '''
    Returns True if argument is palindrome.
    
    >>> is_palindrome('')
    True
    
    >>> is_palindrome('a')
    True
    
    >>> is_palindrome('anna')
    True
    
    >>> is_palindrome('oda')
    False
    '''
    
    return text == text[::-1]
