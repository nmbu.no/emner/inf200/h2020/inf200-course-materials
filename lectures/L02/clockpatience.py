import random

C = ['Hearts', 'Clubs', 'Spades', 'Diamonds']
V = range(1, 14)

deck = [(c, v) for c in C for v in V]

print(deck)

random.shuffle(deck)

print(deck)

table = list([] for _ in range(13))

print(table)

pos = 0
while deck:
    card = deck.pop()
    table[pos].append(card)
    pos += 1
    if pos > 12:
        pos = 0

print(table)