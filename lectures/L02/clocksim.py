# -*- coding: utf-8 -*-

"""

"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import random

SUITS = ('C', 'S', 'H', 'D')
VALUES = range(1, 14)


def play_game():
    deck = [(s, v) for s in SUITS for v in VALUES]

    random.shuffle(deck)

    stacks = [[] for _ in range(13)]
    locked = [False] * 13
    position = 0

    while deck and not all(locked):
        card = deck.pop()
        while locked[position]:
            position = (position + 1) % 13

        stacks[position].append(card)
        locked[position] = (position + 1) == card[1]

        position = (position + 1) % 13

    return all(locked), stacks


results = [play_game()[0] for _ in range(10000)]
print(sum(results))

if __name__ == "__main__":
    success = False
    while not success:
        success, stacks = play_game()

    for s in stacks:
        print(s)
