# -*- coding: utf-8 -*-

"""
Matplotlib GUI Demo for INF200.

This Python script provides a simple demo of how to construct a graphical
user interface using Matplotlib widgets.

The demo does not do very much:

- It creates a figure with GUI elements (widgets).
- Each widget is connected to a callback function.
- Each callback function provides some output indicating user action.
- Widgets are:
    - Axes for plotting with possibility to mark rectangular areas
    - A Quit button
    - A Reset button
    - A slider to set a parameter

For a more meaningful example, see project mandel_gui.

Note:
    Before running this code from within PyCharm, you need to go to
    Settings > Tools > Python Scientific and remove the checkmark for
    "Show plots in tool window". Otherwise, PyCharm will display figures
    in the right sidebar without possibility for interaction.

References and sources:
    https://matplotlib.org/gallery/widgets/slider_demo.html
    https://matplotlib.org/users/event_handling.html
    https://matplotlib.org/api/widgets_api.html
    https://matplotlib.org/examples/widgets/buttons.html
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import RectangleSelector, Button, Slider


class DemoGui:
    """
    A demonstration GUI with four widgets.

    The GUI plots a sine function. The period can be adjusted with a slider.
    All callback functions print information to the console.

    The constructor builds the GUI, but you need to call the show() method
    to display the GUI and work with it.
    """

    def __init__(self):

        # ---------------------------------------------

        # Model data
        # default and current parameter value
        self._k_default = 1
        self._k = self._k_default

        # x-axis values for plotting
        self._x_vals = np.linspace(0, 10, num=1000)

        # ---------------------------------------------

        # Create a figure, which will hold the entire GUI
        self._fig = plt.figure()

        # Add a main axis for plotting, and additional axis for the other
        # GUI elements
        self._ax_plot = self._fig.add_axes([0.1, 0.15, 0.85, 0.8])
        self._ax_slider = self._fig.add_axes([0.15, 0.03, 0.2, 0.05])
        self._ax_reset = self._fig.add_axes([0.7, 0.03, 0.1, 0.05])
        self._ax_quit = self._fig.add_axes([0.85, 0.03, 0.1, 0.05])

        # ---------------------------------------------

        # line object, not showing anything yet
        self._line = self._ax_plot.plot(self._x_vals,
                                        np.NaN * np.ones_like(self._x_vals))[0]
        self._ax_plot.set_xlim(self._x_vals.min(), self._x_vals.max())
        self._ax_plot.set_ylim(-1.05, 1.05)

        # ---------------------------------------------

        # Add widgets to the axes. We need to assign the widgets
        # to member variables so they are not deleted by garbage collection.

        # Widget allowing user to mark a rectangle in the plot
        self._w_rect = RectangleSelector(self._ax_plot,    # axes to attach to
                                         self._on_select,  # method to call
                                         drawtype='box')

        # ..............................................

        # Widget for the slider, also define initial parameter value
        self._w_slide = Slider(self._ax_slider,
                               label='k',  # label for slider
                               valmin=0, valmax=10,  # minimal, maximal value
                               valinit=self._k,      # initial value
                               valfmt='%.2f')        # format for value display
        # Tell slider widget what to do when user changes value
        self._w_slide.on_changed(self._set_n)

        # ..............................................

        # Widget for reset button, and action to perform
        self._w_reset = Button(self._ax_reset, 'Reset')
        self._w_reset.on_clicked(self._reset)

        # ..............................................

        # Widget for quit button, and action to perform
        self._w_quit = Button(self._ax_quit, 'Quit')
        self._w_quit.on_clicked(self._quit)

    def show(self):
        """
        Displays the GUI and starts infinite loop.

        To exit, the user needs to push the Quit button.
        """

        self._update()   # make sure plot is up-to-date
        plt.show()

    def _on_select(self, ev_click, ev_release):
        """
        Respond to rectangular selection.

        :param ev_click: Event describing mouse-button press
        :param ev_release: Event describing mouse-button release
        """

        print('\nRectangular Selection')
        print('    First corner: ({0.xdata}, {0.ydata})'.format(ev_click))
        print('    Last  corner: ({0.xdata}, {0.ydata})'.format(ev_release))
        print('    Button      : {0.button}'.format(ev_click))

    def _set_n(self, value):
        """
        Set parameter to new value

        :param value: New value for n from slider widget.
        """

        print('\nNew Value ')
        print('    Value: {}'.format(value))

        self._k = value  # value from slider is always float
        self._update()

    def _reset(self, event):
        """
        Reset parameters and plot.

        :param event: Event describing button click
        """

        print('\nReset button clicked')
        print('    {}'.format(event))

        self._k = self._k_default
        self._w_slide.reset()
        self._update()

    def _quit(self, event):
        """
        Close figure and exit

        :param event: Event describing button click
        """

        print('\nQuit button clicked')
        print('    {}'.format(event))

        plt.close(self._fig)
        print('\nGood bye!')

    def _update(self):
        """
        Update graphic according to current parameter value.

        :return:
        """

        y = np.sin(self._k * self._x_vals)
        self._line.set_ydata(y)
        self._fig.canvas.draw_idle()


if __name__ == '__main__':
    dg = DemoGui()
    dg.show()
