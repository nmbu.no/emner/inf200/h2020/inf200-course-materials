# -*- coding: utf-8 -*-

"""
GUI for exploring Mandelbrot sets.

Note:
    Before running this code from within PyCharm, you need to go to
    Settings > Tools > Python Scientific and remove the checkmark for
    "Show plots in tool window". Otherwise, PyCharm will display figures
    in the right sidebar without possibility for interaction.

    This version of the code contains no documentation for use in the lecture.
    See mandel_gui_with_doc.py for a documented version.

References and sources:
    https://matplotlib.org/gallery/widgets/slider_demo.html
    https://matplotlib.org/users/event_handling.html
    https://matplotlib.org/api/widgets_api.html
    https://matplotlib.org/examples/widgets/buttons.html

For more ideas, see
    https://www.ibm.com/developerworks/community/blogs/jfp/entry/My_Christmas_Gift?lang=en
    https://www.ibm.com/developerworks/community/blogs/jfp/entry/How_To_Compute_Mandelbrodt_Set_Quickly?lang=en
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import numba
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmaps
import matplotlib.colors as colors
from matplotlib.widgets import RectangleSelector, Button, Slider, RadioButtons


class MandelModel:
    DEFAULT_ITERATIONS = 100
    DEFAULT_SIZE = 1000

    def __init__(self, nx=DEFAULT_SIZE, ny=DEFAULT_SIZE):
        self._nx = nx
        self._ny = ny
        self.reset()

    def reset(self):
        self._x = np.linspace(-2, 1, num=self._nx)
        self._y = np.linspace(-1.5, 1.5, num=self._ny)

        self._iterations = self.DEFAULT_ITERATIONS

        self._data = np.empty((self._nx, self._ny))
        self._data_valid = False

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def data(self):
        if not self._data_valid:
            self._mandel_driver()
        return self._data

    @property
    def iterations(self):
        return self._iterations

    @iterations.setter
    def iterations(self, value):
        if value < 1:
            raise ValueError('Number of iterations >= 1 required.')
        self._iterations = int(value)
        self._data_valid = False

    def set_limits(self, xmin, xmax, ymin, ymax):
        if xmin >= xmax:
            raise ValueError('xmin < xmax required.')
        if ymin >= ymax:
            raise ValueError('ymin < ymax required.')

        self._x = np.linspace(xmin, xmax, num=self._nx)
        self._y = np.linspace(ymin, ymax, num=self._ny)
        self._data_valid = False

    @staticmethod
    @numba.jit
    def _mandel_iter(x, y, max_iter):
        c = complex(x, y)
        z = 0.0j
        for n in range(max_iter):
            z = z * z + c
            if abs(z) > 2:
                return n + 1
        return max_iter

    @numba.jit
    def _mandel_driver(self):
        for i, y in enumerate(self.y):
            for j, x in enumerate(self.x):
                self._data[i, j] = self._mandel_iter(x, y, self._iterations)
        self._data_valid = True


class MandelGui:
    COLORMAPS =['viridis', 'inferno', 'inferno_r', 'hot', 'seismic', 'ocean',
                'jet', 'jet_r']
    DEFAULT_COLORMAP = 'inferno'

    def __init__(self, model):

        self._model = model

        self._fig = plt.figure()

        self._ax_plot = self._fig.add_axes([0.1, 0.15, 0.7, 0.8])
        self._ax_iters = self._fig.add_axes([0.15, 0.03, 0.3, 0.03])
        self._ax_cmap = self._fig.add_axes([0.82, 0.4, 0.15, 0.5])
        self._ax_reset = self._fig.add_axes([0.82, 0.15, 0.15, 0.05])
        self._ax_quit = self._fig.add_axes([0.82, 0.05, 0.15, 0.05])

        self._w_rect = RectangleSelector(self._ax_plot,
                                         self._select_zoom,
                                         useblit=True,
                                         drawtype='box')

        self._w_iters = Slider(self._ax_iters,
                               label='Iterations',
                               valmin=0, valmax=1000,
                               valinit=model.iterations,
                               valstep=1,
                               valfmt='%.0f')
        self._w_iters.on_changed(self._set_iters)

        self._cmap = self.DEFAULT_COLORMAP
        self._w_cmap = RadioButtons(self._ax_cmap,
                                    labels=self.COLORMAPS)
        self._w_cmap.set_active(self.COLORMAPS.index(self._cmap))
        self._w_cmap.on_clicked(self._set_cmap)

        self._w_reset = Button(self._ax_reset, 'Reset')
        self._w_reset.on_clicked(self._reset)

        self._w_quit = Button(self._ax_quit, 'Quit')
        self._w_quit.on_clicked(self._quit)

    def show(self):
        self._update()
        plt.show()

    def _select_zoom(self, ev_click, ev_release):
        self._model.set_limits(min(ev_click.xdata, ev_release.xdata),
                               max(ev_click.xdata, ev_release.xdata),
                               min(ev_click.ydata, ev_release.ydata),
                               max(ev_click.ydata, ev_release.ydata))
        self._update()

    def _set_iters(self, value):
        self._model.iterations = value
        self._update()

    def _set_cmap(self, label):
        self._cmap = label
        self._update()

    def _reset(self, event):
        self._model.reset()
        self._cmap = self.DEFAULT_COLORMAP
        self._w_iters.set_val(self._model.iterations)
        self._w_cmap.set_active(self.COLORMAPS.index(self._cmap))
        self._update()

    def _quit(self, event):
        plt.close(self._fig)

    def _update(self):
        self._ax_plot.clear()
        self._ax_plot.pcolormesh(self._model.x,
                                 self._model.y,
                                 self._model.data,
                                 cmap=cmaps.get_cmap(self._cmap),
                                 norm=colors.LogNorm())
        self._ax_plot.set_aspect('equal')

        self._fig.canvas.draw_idle()


if __name__ == '__main__':
    mmodel = MandelModel()
    MandelGui(mmodel).show()
