import numba
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
plt.rcParams['figure.figsize'] = (8, 8)

def mandel_plot(x, y, m, figsize=(8, 8), cmap=cm.jet_r):
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.pcolormesh(x, y, m, cmap=cmap, norm=colors.LogNorm(), shading='nearest');
    ax.set_aspect('equal');


@numba.jit
def mandel_iter(x, y, max_iter):
    c = complex(x, y)
    z = 0.0j
    for n in range(max_iter):
        z = z * z  + c
        if abs(z) > 2:
            return n + 1
    return max_iter

def mandel_driver(xmin, xmax, ymin, ymax, width, height, max_iter=500, plot=True):
    x = np.linspace(xmin, xmax, num=width)
    y = np.linspace(ymin, ymax, num=height)
    r = np.empty((height, width))
    for i in range(height):
        for j in range(width):
            r[i, j] = mandel_iter(x[j], y[i], max_iter)
    if plot:
        mandel_plot(x, y, r)
    return r, x, y

mandel_driver(-2, 1, -1.5, 1.5, 500, 500, max_iter=500);

plt.show()
