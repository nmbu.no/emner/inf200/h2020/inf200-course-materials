# Sample simulation results

Scripts are in directory checks.

- check_sim_??????.pdf: result of check_sim.py with two different seeds
- mono_hc_?????_00001.png: result of mono_hc.py for three different seeds
- mono_ho_?????_00001.png: result of mono_ho.py for three different seeds
