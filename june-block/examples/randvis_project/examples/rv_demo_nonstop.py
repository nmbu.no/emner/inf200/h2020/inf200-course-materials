#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a small demo script running a randvis simulation.
"""

__author__ = "Hans E Plesser / NMBU"

import matplotlib.pyplot as plt
from randvis.simulation import DVSim

if __name__ == '__main__':

    sim = DVSim((10, 15), 0.1, 12345, {'max': 1.0, 'delta': 0.05})
    sim.simulate(250, 1, 5)

    print('Close the figure to end the program!')

    plt.show()
