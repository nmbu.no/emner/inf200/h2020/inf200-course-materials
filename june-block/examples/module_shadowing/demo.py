import importlib
import sys

print('-' * 92)
print(f'Do we have math yet: {"math" in sys.modules}')

print('-' * 92)
for p in sys.path:
    print(p)

print('-' * 92)
print(importlib.util.find_spec('math'))  # noqa

preloaded_modules = sorted(sys.modules.keys())
print('-' * 92)
print(f'{len(preloaded_modules)} preloaded modules')
print('-' * 92)

at_end_of_line = False
for ix, m in enumerate(preloaded_modules):
    print(f'{m[:20]:23}', end='')
    at_end_of_line = ix % 4 == 3
    if at_end_of_line:
        print()

if not at_end_of_line:
    print()

print('-' * 105)

import math  # noqa

print(math.__file__)
print(math.sin(1))  # noqa
