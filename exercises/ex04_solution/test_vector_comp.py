# -*- coding: utf-8 -*-

"""
Exercise 04-C. Tests for Vector class comparison operators.
"""

import pytest
from vector_comp import Vector

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def test_equal():
    """Test that comparisons for equality work."""

    assert (Vector(0, 0) == Vector(0, 0))
    assert (Vector(-1, 1) == Vector(-1, 1))
    assert (not (Vector(0, 1) == Vector(1, 0)))


def test_not_equal():
    """Test that comparisons for inequality work."""

    assert Vector(0, 0) != Vector(1, 0)
    assert Vector(-1, 1) != Vector(1, 1)
    assert Vector(-1, 1) != Vector(1, -1)
    assert not (Vector(1, 1) != Vector(1, 1))


def test_undefined_comparisons():
    """Test that undefined comparisons raise exception."""

    v = Vector(1, 2)
    w = Vector(2, 3)

    with pytest.raises(TypeError):
        v < w

    with pytest.raises(TypeError):
        v <= w

    with pytest.raises(TypeError):
        v > w

    with pytest.raises(TypeError):
        v >= w
