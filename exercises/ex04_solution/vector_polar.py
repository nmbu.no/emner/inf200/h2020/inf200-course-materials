# -*- coding: utf-8 -*-

"""
Exercise 04-B. Vector class with polar-coordinate implementation.
"""

import math

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


# Code below is modified from INF200_H20_L07.ipynb by Hans Ekkehard Plesser, NMBU
class Vector:

    def __init__(self, x, y):
        self.r = math.sqrt(x**2 + y**2)
        self.phi = math.atan2(y, x)

    def _to_xy(self):
        return self.r * math.cos(self.phi), self.r * math.sin(self.phi)

    def __repr__(self):
        x, y = self._to_xy()
        return f'Vector({x}, {y})'

    def __add__(self, rhs):
        x, y = self._to_xy()
        rx, ry = rhs._to_xy()
        return Vector(x + rx, y + ry)

    def __sub__(self, rhs):
        return self + (-1.0 * rhs)

    def __mul__(self, rhs):
        res = Vector(0, 0)

        if rhs < 0:
            rhs = -rhs
            res.phi = (self.phi + math.pi) % (2*math.pi)
        else:
            res.phi = self.phi
        res.r = rhs * self.r

        return res

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        return self * (1. / rhs)

    def norm(self):
        return self.r


if __name__ == '__main__':
    print(Vector(1, 0) + Vector(0, 1))
    print(Vector(1, 0) - Vector(0, 1))

    v = Vector(1, 0)
    vm = -1 * v
    print(v + vm)
    print(v.norm())
    print(vm.norm())

