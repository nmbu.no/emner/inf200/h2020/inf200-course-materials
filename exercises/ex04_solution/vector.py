# -*- coding: utf-8 -*-

"""
Exercise 04-A. Vector class.
"""

import math

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


# Code below is copied from INF200_H20_L07.ipynb by Hans Ekkehard Plesser, NMBU
class Vector:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Vector({0.x}, {0.y})'.format(self)

    def __add__(self, rhs):
        return Vector(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return Vector(self.x - rhs.x, self.y - rhs.y)

    def __mul__(self, rhs):
        return Vector(self.x * rhs, self.y * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __truediv__(self, rhs):
        return self * (1. / rhs)

    def norm(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)
