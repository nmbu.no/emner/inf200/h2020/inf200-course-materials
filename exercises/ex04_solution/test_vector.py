# -*- coding: utf-8 -*-

"""
Exercise 04-A. Tests for Vector class.
"""

import pytest
from vector import Vector

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


@pytest.mark.parametrize('a, b, expected',
                         [(Vector(0, 0), Vector(0, 0), Vector(0, 0)),
                          (Vector(0, 0), Vector(1, 2), Vector(1, 2)),
                          (Vector(3, 4), Vector(2, -1), Vector(5, 3))])
def test_add_vectors(a, b, expected):
    """Test that two vectors are added correctly."""

    res = a + b
    assert res.x == expected.x and res.y == expected.y


@pytest.mark.parametrize('a, b, expected',
                         [(Vector(0, 0), Vector(0, 0), Vector(0, 0)),
                          (Vector(0, 0), Vector(1, 2), Vector(-1, -2)),
                          (Vector(3, 4), Vector(2, -1), Vector(1, 5))])
def test_subtract_vectors(a, b, expected):
    """Test that two vectors are subtracted correctly."""

    res = a - b
    assert res.x == expected.x and res.y == expected.y


@pytest.mark.parametrize('a, b, expected',
                         [(Vector(0, 0), 5, Vector(0, 0)),
                          (Vector(1, 2), 4, Vector(4, 8)),
                          (4, Vector(5, 6), Vector(20, 24))])
def test_scale_vectors(a, b, expected):
    """Test that vector-scalar multiplications works correctly"""

    res = a * b
    assert res.x == expected.x and res.y == expected.y


@pytest.mark.parametrize('a, b, expected',
                         [(Vector(0, 0), 5, Vector(0, 0)),
                          (Vector(4, 8), 4, Vector(1, 2)),
                          (Vector(4, 8), -4, Vector(-1, -2))])
def test_divide_vectors(a, b, expected):
    """Test that division of vector by scalar works correctly"""

    res = a / b
    assert res.x == expected.x and res.y == expected.y


@pytest.mark.parametrize('a, expected',
                         [(Vector(0, 0), 0),
                          (Vector(3, 4), 5),
                          (Vector(-3, 4), 5)])
def test_vector_norm(a, expected):
    """Test that norm of vector is computed correctly."""
    assert a.norm() == expected
