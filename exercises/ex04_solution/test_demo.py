import pytest

@pytest.mark.parametrize('a, b, expected', [(0, 0, 0), (1, 2, 3), (5, -3, 2)])
def test_add(a, b, expected):
    assert a + b == expected
