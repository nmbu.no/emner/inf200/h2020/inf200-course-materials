# -*- coding: utf-8 -*-

"""
Exercise 02-A: Counting byte frequencies in a file.

Part of a course at Norges miljø- og biovitenskapelige universitet på Ås.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def byte_counts(text_filename):
    counts = [0] * 256
    with open(text_filename, 'rb') as infile:
        for byt in infile.read():
            counts[int(byt)] += 1
    return counts


if __name__ == '__main__':

    filename = 'file_stats.py'
    frequencies = byte_counts(filename)
    for code in range(256):
        if frequencies[code] > 0:
            print('{:3d}{:>3s}{:6d}'.format(code,
                                            chr(code) if 32 < code < 128 else '',
                                            frequencies[code]))
