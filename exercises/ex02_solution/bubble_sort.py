# -*- coding: utf-8 -*-

"""
Exercise 02-B: Bubble sort

Part of a course at Norges miljø- og biovitenskapelige universitet på Ås.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def bubble_sort(in_data):
    """
    Return sorted copy of in_data.
    """
    
    s_data = list(in_data)
    for j in reversed(range(len(s_data))):
        for k in range(j):
            if s_data[k+1] < s_data[k]:
                s_data[k], s_data[k+1] = s_data[k+1], s_data[k]
    return s_data


if __name__ == "__main__":

    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print('{!s:>15} --> {!s:>15}'.format(data, bubble_sort(data)))
