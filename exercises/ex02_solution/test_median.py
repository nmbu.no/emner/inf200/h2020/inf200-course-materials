# -*- coding: utf-8 -*-

"""
Exercise 02-A: Tests for median function.

Part of a course at Norges miljø- og biovitenskapelige universitet på Ås.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """

    s_data = sorted(data)
    n = len(s_data)
    return (s_data[n // 2] if n % 2 == 1
            else 0.5 * (s_data[n // 2 - 1] + s_data[n // 2]))


def test_single_element():
    """Correct result for single-element list?"""

    return median([5]) == 5


def test_input_unchanged():
    """Test that argument passed to median remains unchanged."""

    data = [4, 5, 1, 3, 2]
    median(data)
    return data == [4, 5, 1, 3, 2]


def test_odd_num_elements():
    """Test median for lists with odd number of elements."""

    ok1 = median([3, 1, 2]) == 2
    ok2 = median([3, 1, 2, 3, 5]) == 3

    return ok1 and ok2


def test_even_num_elements():
    """Test median for lists with even number of elements."""

    ok1 = median([3, 1, 2, 4]) == 2.5
    ok2 = median([3, 2, 1, 4, 3, 5]) == 3

    return ok1 and ok2


def test_sorted_elements():
    """Test median for list with sorted elements."""
    
    ok1 = median([1, 2, 3]) == 2
    ok2 = median([1, 2, 3, 4]) == 2.5

    return ok1 and ok2


def test_reverse_elements():
    """Test median for list with reverse-sorted elements."""

    ok1 = median([3, 2, 1]) == 2
    ok2 = median([4, 3, 2, 1]) == 2.5

    return ok1 and ok2


def test_tuple_argument():
    """Test that median function works for tuples, too."""

    ok1 = median((1, 2, 3)) == 2
    ok2 = median((1, 2, 3, 4)) == 2.5

    return ok1 and ok2


if __name__ == '__main__':
    test_suite = [test_single_element,
                  test_input_unchanged,
                  test_odd_num_elements,
                  test_even_num_elements,
                  test_sorted_elements,
                  test_reverse_elements,
                  test_tuple_argument]
    for test in test_suite:
        passed = test()
        result = 'PASS' if passed else 'FAIL'
        print('{:30s}: {}'.format(test.__name__, result))
