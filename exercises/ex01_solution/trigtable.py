# -*- coding: utf-8 -*-

"""
Exercise 01-A. Tabulate trigonometric functions
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


import math

def tan_or_inf(phi, inf_bound=1e9):
    """
    Return tangent if within bounds, otherwise infinity.

    phi: Angle in radian
    inf_bound: Any absolute values beyond will be returned as +-inf
    """

    tan_phi = math.tan(phi)
    if abs(tan_phi) <= inf_bound:
        return tan_phi
    elif tan_phi > 0:
        return math.inf
    else:
        return -math.inf

    
def print_trig_table(min_deg, max_deg, delta_deg):
    """
    Print trigonometric table.

    min_deg: smallest angle to tabulate (0..360)
    max_deg: largest angle to tabluate (0..360)
    delta_deg: step between table entries
    """

    # ensure code works for non-integer deg
    num_rows = (max_deg - min_deg) // delta_deg + 1
    num_cols = 4
    col_width = 10
    tab_width = num_cols * col_width
    separator_line = '-' * tab_width
    
    print(separator_line)
    print('{:>10s}{:>10s}{:>10s}{:>10s}'.format(
               'phi', 'sin(phi)', 'cos(phi)', 'tan(phi)'))
    print(separator_line)

    for row in range(num_rows):
        phi = ( min_deg + row * delta_deg )
        if phi > max_deg:
            break

        phi_rad = math.pi * phi / 180

        print('{:10.1f}{:10.3f}{:10.3f}{:10.3f}'.format(phi,
                                                        math.sin(phi_rad),
                                                        math.cos(phi_rad),
                                                        tan_or_inf(phi_rad)))

    print(separator_line)

if __name__ == '__main__':
    print_trig_table(0, 360, 5)
