# -*- coding: utf-8 -*-

"""
Example solution to EX05 Task C.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def log_in_out(func):
    """
    Decorator printing argument to and result of func.

    :param func: Callable taking a single argument.
    :returns: wrapped callable
    """

    def wrapped(x):
        print('Argument:', x)
        res = func(x)
        print('Result  :', res)
        return res

    return wrapped


@log_in_out
def square(x):
    return x * x


if __name__ == '__main__':
    square(0)
    square(5)


