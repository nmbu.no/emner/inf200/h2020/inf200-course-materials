# -*- coding: utf-8 -*-

"""
Example solution to EX05 Task A.
"""

import re

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def select_int(filename):
    """
    Returns list with all lines containing "int".
    """

    with open(filename, 'r') as text:
        return [line.strip()
                for line in text
                if re.search('int', line)]


def select_at(filename):
    """
    Returns list with all lines containing the word "at".
    """

    with open(filename, 'r') as text:
        return [line.strip()
                for line in text
                if re.search(r'\b[Aa]t\b', line)]


if __name__ == '__main__':
    print('INT')
    print(select_int('replace.py'))
    print()
    print('AT')
    print(select_at('replace.py'))
