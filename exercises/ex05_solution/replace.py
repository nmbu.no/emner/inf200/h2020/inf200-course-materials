# -*- coding: utf-8 -*-

"""
Example solution to EX05 Task B.
"""

import re

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


def upper_cat(filename):
    """
    Returns list with all lines, "cat" replaced by "CAT".
    """

    with open(filename, 'r') as text:
        return [re.sub('cat', 'CAT', line).strip()
                for line in text]


if __name__ == '__main__':
    for line in upper_cat('replace.py'):
        print(line)
