# -*- coding: utf-8 -*-

"""
Exercise 03-C. Test letter frequency counting code.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@nmbu.no'


from letter_counts import letter_freq


def test_empty_string():
    """
    Empty dictionary shall be returned for an empty string.
    """

    assert letter_freq('') == {}


def test_uppercase():
    """
    All letters should be counted as lowercase letters.
    """

    assert letter_freq('aAaA') == {'a': 4}


def test_uppercase_non_ascii():
    """
    Check conversion to lowercase works for non-ASCII characters.
    """

    assert letter_freq('æøåØÆÅ') == {'æ': 2, 'ø': 2, 'å': 2}


def test_string_plain_characters():
    """
    Test string with ASCII characters.
    """

    assert letter_freq('This is a plain text.') == {' ': 4, '.': 1,
                                                    'a': 2, 'e': 1, 'h': 1, 'i': 3, 'l': 1,
                                                    'n': 1, 'p': 1, 's': 2, 't': 3, 'x': 1}


def test_string_emojis():
    """
    Test string with emojis.
    """

    # Note: Entering the emojis here was tricky, sometimes the system entered hidden spaces.
    #       It needed careful inspection and correction to make sure only the actual emojis
    #       were in place.
    assert letter_freq('😃🤨🍍🐶🍏🐶🍏🍏') == {'😃': 1, '🤨': 1, '🍍': 1, '🐶': 2, '🍏': 3}
